import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Item {
    property var codecsModel: ListModel {}
    property var monoFontFamily: Theme.fontFamily

    property var comboBox: undefined
    property var textArea: undefined
    property var passwordField: undefined

    property bool passwordMode: false

    property var fastSourceText:
        passwordMode ? passwordInputField.text : inputField.text

    property var sourceText:
        passwordMode ? passwordInputField.slowText : inputField.slowText

    property var comboBoxLabel: undefined
    property var comboBoxDescription: undefined
    property var textAreaLabel: undefined
    property var textAreaPlaceholderText: undefined

    property var examineSectionText: undefined
    property var examineComboBoxLabel: undefined
    property var examineComboBoxDescription: undefined
    property var examineTextAreaLabel: undefined
    property var examineTextAreaPlaceholderText: undefined

    property var encoding: comboBox ? comboBox.encoding : undefined
    property var initialInputEncodingElement: function(e){return true}
    property var initialExamineEncodingElement: function(e){return true}
    property bool empty: sourceText == ""

    property bool examineInput: false

    height: column.height
    width: parent.width

    signal examinedTextCopied(var description)

    function clear () { if(textArea) { textArea.clear() } }

    Column {
        id: column
        spacing: Theme.paddingMedium
        width: parent.width

        PasswordField {
            id: passwordInputField
            visible: passwordMode
            property var slowText: ""
            property bool empty: text == ""
            label: textAreaLabel || (qsTranslate("textarea-label","Input")
                + (!examineInput ?
                    (" - " + qsTranslate("textarea-placeholder-text",
                        "long press to examine")
                    ) : ""))
            placeholderText: textAreaPlaceholderText ||
                qsTranslate("textarea-placeholder-text", "Enter text")
            font.family: (inputEncodingComboBox.encoding ?
                (inputEncodingComboBox.encoding.indexOf("_sep")!=-1 )
                    : false) ?  monoFontFamily : Theme.fontFamily
            font.pixelSize: Theme.fontSizeTiny +
                (Theme.fontSizeMedium - Theme.fontSizeTiny)
                / ( 1 + text.length / 1000 )
            width: parent.width
            function clear () { text = "" }
            Component.onCompleted: {
                passwordField = passwordInputField
                passwordModeChanged.connect(
                    function(m){if(m){text = inputField.text}})
            }
            onFocusChanged: { if(!focus) {
                if(slowText != text) { slowText = text }
                } }
            onPressAndHold: { examineInput = true }
        }

        TextArea {
            id: inputField
            property bool empty: text == ""
            property var slowText: ""
            visible: ! passwordMode
            label: passwordInputField.label
            placeholderText: passwordInputField.placeholderText
            font.family: passwordInputField.font.family
            font.pixelSize: passwordInputField.font.pixelSize
            width: parent.width
            function clear () { text = "" }
            Component.onCompleted: {
                textArea = inputField
                passwordModeChanged.connect(
                    function(m){if(m){text = passwordInputField.text}})
            }
            onFocusChanged: { if(!focus) {
                if(slowText != text) { slowText = text }
                } }
            onPressAndHold: { examineInput = true }
        }

        ModelComboBox {
            id: inputEncodingComboBox
            property var encoding: rawValue
            selectionModel: codecsModel
            enabled: parent.enabled
            elementToDisplayText: function(e){return e.displayname}
            elementToRawText: function(e){return e.name}
            elementEnabled: function(e){return e.name.indexOf("_sep") == -1}
            initialElement: initialInputEncodingElement
            label: comboBoxLabel ||
                qsTranslate("combobox-label","Input Encoding")
            description: comboBoxDescription ||
                qsTranslate("combobox-description",
                    "How to interpret the input")
            width: parent.width
            visible: fastSourceText != "" || focus
            Component.onCompleted: { comboBox = inputEncodingComboBox }
        }


        SectionHeader {
            text: examineSectionText || qsTranslate("section", "Examine Input")
            visible: examineInput
        }

        EncodingTransformer {
            id: inputExamineField
            width: parent.width
            // cannot use toplevel codecsModel variable due to binding loop
            // inputEncodingComboBox.selectionModel is the same...
            codecsModel: inputEncodingComboBox.selectionModel
            inputText: fastSourceText
            inputEncoding: inputEncodingComboBox.encoding
            inputEncodingDisplayText: inputEncodingComboBox.value
            initialOutputEncodingElement: initialExamineEncodingElement
            visible: examineInput
            active: examineInput
            onTextCopied: { examinedTextCopied(description) }
            comboBoxLabel: examineComboBoxLabel
            comboBoxDescription: examineComboBoxDescription
            textAreaLabel: examineTextAreaLabel
            textAreaPlaceholderText: examineTextAreaPlaceholderText
        }

        Button {
            id: examineToggleButton
            visible: examineInput
            enabled: parent.enabled
            anchors.horizontalCenter: parent.horizontalCenter
            text: examineInput ? qsTranslate("button-text","Hide")
                : qsTranslate("button-text","Examine")
            onClicked: { examineInput = ! examineInput }
        }


    }

    Component.onCompleted: {
        // find first monospace font
        Qt.fontFamilies().some(function(family){
            if(family.toLowerCase().indexOf("mono") != -1) {
                monoFontFamily = family
                return true
            }
        })
    }

}
