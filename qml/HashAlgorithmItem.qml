import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Item {
    id: root
    height: column.height
    width: parent.width

    property var codecsModel: ListModel {}
    property var algorithmsModel: ListModel {}
    property var algorithmsJson: {}
    property var algorithmsData: {}
    property var algorithmInfo: {}
    property var algorithmParamsDefinition: []
    property var algorithmParams: {}

    signal textCopied(var description)
    signal invalidItemSelected()

    onAlgorithmsJsonChanged: {
        algorithmsData = JSON.parse(algorithmsJson)
    }

    onAlgorithmsDataChanged: {
        algorithmInfo =
            (algorithmsData[hashAlgorithmComboBox.currentIndex] || {})
    }

    onAlgorithmInfoChanged: {
        algorithmParamsDefinition = (algorithmInfo||{}).params
        algorithmParams = {
            "returns":(algorithmInfo||{}).returns,
            "name": (algorithmInfo||{}).name,
            "displayname": (algorithmInfo||{}).displayname,
            "params":{}
        }
    }

    property var comboBox: undefined

    property var comboBoxInitialElement: undefined
    property var comboBoxLabel: undefined
    property var comboBoxDescription: undefined

    property var paramsModel: ListModel {}
    property var paramsRepeater: undefined


    Column {
        id: column
        spacing: Theme.paddingMedium
        width: parent.width

        ModelComboBox {
            id: hashAlgorithmComboBox
            selectionModel: algorithmsModel
            elementToDisplayText: function(e){return e.displayname}
            elementToRawText:     function(e){return e.name}
            initialElement: comboBoxInitialElement || function(e){
                return e.name.toLowerCase().indexOf("simple")!=-1}
            label: comboBoxLabel || qsTranslate("first-page","Method")
            enabled: parent.enabled
            description: comboBoxDescription ||
                qsTranslate("first-page", "Method to use")
            onCurrentIndexChanged: {
                algorithmsDataChanged()
            }
            Component.onCompleted: {
                root.comboBox = hashAlgorithmComboBox
            }
        }

        MouseArea {
            width: parent.width
            height: hashAlgorithmDescriptionLabel.height
            visible: algorithmInfo.description ? true : false
            MonochromeIcon {
                id: infoIcon
                source: "image://theme/icon-m-about"
                color: Theme.secondaryColor
                x: Theme.horizontalPageMargin
                anchors {
                    verticalCenter:
                        hashAlgorithmDescriptionLabel.verticalCenter
                }
            }
            Label {
                id: hashAlgorithmDescriptionLabel
                property bool minimized: false
                readonly property int minimizedLineCount: 3
                IntValidator {id: intBounds} // just to have the int bounds
                anchors {
                    left: infoIcon.right
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                horizontalAlignment:
                    minimized ? Text.AlignLeft : Text.AlignJustify
                font.pixelSize: Theme.fontSizeSmall
                text: algorithmInfo.description || ""
                wrapMode: Text.WordWrap
                maximumLineCount:
                    minimized ? minimizedLineCount : intBounds.top
                truncationMode:
                    minimized ? TruncationMode.Fade : TruncationMode.None
                color: Theme.secondaryColor
                onTextChanged: {
                    minimized = false
                    minimized = lineCount > minimizedLineCount
                }
            }
            onClicked: {
                hashAlgorithmDescriptionLabel.minimized =
                    ! hashAlgorithmDescriptionLabel.minimized
            }
        }

        Repeater {
            id: repeater
            model: paramsModel
            delegate : Column {
                id: repeaterColumn
                spacing: Theme.paddingSmall
                width: parent.width
                property var paramDisplayName: "%1 - %2"
                    .arg(algorithmInfo.displayname).arg(model.displayname)
                property var paramDescription:
                    (paramsModel.get(index)||{}).description || ""
                property bool showDescription: false

                MouseArea {
                    width: parent.width
                    height: paramDisplayNameSection.height
                    SectionHeader {
                        id: paramDisplayNameSection
                        text: paramDisplayName
                        MonochromeIcon {
                            id: paramDescriptionInfoIcon
                            source: "image://theme/icon-s-message"
                            visible: !showDescription && paramDescription != ""
                            color: Theme.secondaryColor
                            anchors {
                                verticalCenter: parent.verticalCenter
                                left: parent.left
                            }
                        }
                    }
                    onClicked: { showDescription = !showDescription }
                }

                Label {
                    id: paramDescriptionLabel
                    visible: text != "" && showDescription
                    x: Theme.horizontalPageMargin
                    width: parent.width - 2 * x
                    horizontalAlignment: Text.AlignLeft
                    font.pixelSize: Theme.fontSizeSmall
                    text: paramDescription
                    wrapMode: Text.WordWrap
                    color: Theme.secondaryColor
                }

                Component.onCompleted: {
                    // incubateObject() instead of createObject() is smoother
                    // in the UI but has some timing problems and throws errors
                    // on app teardown sometimes...
                    var paramInfo = paramsModel.get(index)
                    switch(paramInfo.type) {
                        case "bytes": {
                            var component =
                                Qt.createComponent("EncodedInputField.qml")
                            var properties = {
                                "codecsModel": codecsModel,
                                "initialInputEncodingElement":
                                    function(e){return e.name.toLowerCase()
                                    .indexOf("utf")!=-1},
                                "initialExamineEncodingElement": function(e){
                                return e.name.toLowerCase()
                                    .indexOf("hex")!=-1 &&
                                  e.name.toLowerCase().indexOf("_sep")==-1},
                                "passwordMode": paramInfo.password === true,
                                "examineSectionText": qsTranslate("section",
                                    "Examine %1").arg(paramDisplayName),
                                "enabled": Qt.binding(
                                    function(){return parent.enabled}),
                            }
                            var object = component.createObject(
                                repeaterColumn,properties)
                            function updateParams() {
                                algorithmParams.params[paramInfo.name] = {
                                    "text": object.sourceText,
                                    "encoding": object.encoding,
                                }
                                algorithmParamsChanged(algorithmParams)
                            }
                            object.examinedTextCopied.connect(
                                function(description){
                                    root.textCopied(description)
                                })
                            object.sourceTextChanged.connect(updateParams)
                            object.encodingChanged.connect(updateParams)
                            break
                        }
                        case "integer": {
                            var component =
                                Qt.createComponent("IntegerSlider.qml")
                            var object = component.createObject(repeaterColumn,
                            {
                                "sliderLabel": paramInfo.displayName,
                                "textFieldLabel": paramInfo.displayName,
                                "textFieldPlaceholderText":
                                    paramInfo.displayName,
                                "minimum": paramInfo.minimum,
                                "maximum": paramInfo.maximum,
                                // ListModel can't handle different types per
                                // key. We wrapped it into another object...
                                "defaultValue":
                                    (paramInfo.defaultValue||{}).value,
                                "enabled": Qt.binding(
                                    function(){return parent.enabled}),
                            })
                            object.onValueChanged.connect(function(){
                                algorithmParams.params[paramInfo.name] =
                                    object.value
                                algorithmParamsChanged(algorithmParams)
                            })
                            algorithmParams[paramInfo.name] = object.value
                            break
                        }
                        case "hashlib-simple-algorithm-name": {
                            var component =
                                Qt.createComponent("ModelComboBox.qml")
                            var properties = {
                                "selectionModel":
                                    python.hashAlgorithmsHashlibSimpleModel,
                                "elementToDisplayText":
                                    function(e){return e.displayname},
                                "elementToRawText": function(e){return e.name},
                                "initialElement": function(e){
                                    return e.name.toLowerCase()
                                        .indexOf("sha512")!=-1},
                                "label": qsTranslate("first-page","Algorithm"),
                                "description": qsTranslate("first-page",
                                    "Hash algorithm to use"),
                                "enabled": Qt.binding(
                                    function(){return parent.enabled}),
                            }
                            var object = component.createObject(
                                repeaterColumn, properties)
                            object.invalidItemSelected.connect(function(){
                               root.invalidItemSelected()
                            })
                            object.rawValueChanged.connect(function(){
                                algorithmParams.params[paramInfo.name] =
                                    object.rawValue
                                algorithmParamsChanged(algorithmParams)
                            })
                            algorithmParams[paramInfo.name] = object.rawValue
                            break
                        }
                        default: {
                            var component = Qt.createComponent(
                                "/usr/lib/qt5/qml/Sailfish/Silica/Label.qml")
                            component.createObject(repeaterColumn, {
                                "text":"??? %1 ???".arg(paramInfo.type),
                                "width": Qt.binding(
                                    function(){return parent.width}),
                                "enabled": Qt.binding(
                                    function(){return parent.enabled}),
                                })
                        }
                    }
                }
            }

            Component.onCompleted: { paramsRepeater = repeater }
        }

    }

     Component.onCompleted: {
         algorithmParams = JSON.parse("{}")
     }

    onAlgorithmParamsDefinitionChanged: {
        if(algorithmParamsDefinition) {
            paramsModel.clear()
            algorithmParamsDefinition.forEach(
                function(e){paramsModel.append(e)})
        }
    }


}
