import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Item {
    id: root
    property var codecsModel: ListModel {}
    property var monoFontFamily: Theme.fontFamily

    property var comboBox: undefined
    property var textArea: undefined

    property var comboBoxLabel: undefined
    property var comboBoxDescription: undefined
    property var textAreaLabel: undefined
    property var textAreaPlaceholderText: undefined

    property var inputText: ""
    property var inputEncoding: undefined
    property var inputEncodingDisplayText: undefined || inputEncoding
    property var outputEncoding: comboBox ? comboBox.rawValue : undefined
    property var initialOutputEncodingElement: function(e){return true}
    property var outputText: outputField ? outputField.text : ""
    property var outputField: undefined

    property bool active: true

    signal textCopied(var description)

    height: column.height
    width: parent.width

    function clear () { if(outputField) { outputField.clear() } }

    Column {
        id: column
        spacing: Theme.paddingMedium
        width: parent.width

        ModelComboBox {
            id: outputEncodingComboBox
            property var encoding: rawValue
            width: parent.width
            selectionModel: codecsModel
            elementToDisplayText: function(e){return e.displayname}
            elementToRawText: function(e){return e.name}
            initialElement: initialOutputEncodingElement
            enabled: outputField.enabled ||
                (outputField.error && !outputField.busy)
            visible: outputField.visible
            label: comboBoxLabel || qsTranslate("combobox-label","Encoding")
            description: comboBoxDescription ||
                qsTranslate("combobox-description",
                "How to display the decoded input")
            Component.onCompleted: {
                root.comboBox = outputEncodingComboBox
            }
        }

        EncodingTransformTextArea {
            id: outputField
            active: root.active
            sourceText: inputText
            sourceEncoding: inputEncoding
            displayEncoding: outputEncodingComboBox.encoding
            width: parent.width
            label: textAreaLabel || qsTranslate("textarea-label",
                "%1-representation of decoded %2 input")
                .arg(outputEncodingComboBox.value)
                .arg(inputEncodingDisplayText)
            placeholderText: textAreaPlaceholderText
                || qsTranslate("textarea-placeholder", "Input")
            readOnly: true
            onClicked: {
                Clipboard.text = text
                textCopied(label)
            }
            Component.onCompleted: {
                root.outputField = outputField
            }
        }


    }

    Component.onCompleted: {
        // find first monospace font
        Qt.fontFamilies().some(function(family){
            if(family.toLowerCase().indexOf("mono") != -1) {
                monoFontFamily = family
                return true
            }
        })
    }

}
