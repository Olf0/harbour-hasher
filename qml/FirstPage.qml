import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: firstpage

    SilicaFlickable {
        id: view
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {}

        PullDownMenu {
            MenuItem {
                text: qsTranslate("first-page","About Hasher")
                onClicked: app.pageStack.push("AboutPage.qml")
            }
        }

        Column {
            id: column
            spacing: Theme.paddingMedium
            width: parent.width

            PageHeader {
                id: header
                width: parent.width
                title: qsTranslate("pageheader","Hasher")
            }

            HashAlgorithmItem {
                id: hashAlgorithmItem
                codecsModel: python.codecsModel
                algorithmsJson: JSON.stringify(python.hashAlgorithmsComposite || "{}")
                algorithmsModel: python.hashAlgorithmsCompositeModel
                enabled: !hashDisplay.busy
                onTextCopied: {
                    notificationBar.success(qsTranslate("notification-bar",
                        "%1 copied to clipboard").arg(description))
                }
                onInvalidItemSelected: {
                    notificationBar.error(qsTranslate("notification-bar",
                        "invalid selection"))
                }
            }

            SectionHeader {
                text: qsTranslate("section", "Result")
            }

            HashDisplayItem {
                id: hashDisplay
                codecsModel: python.codecsModel
                algorithmParams: hashAlgorithmItem.algorithmParams
                initialDisplayEncodingElement:
                    function(e){ return e.name == bridgeEncoding }
                onTextCopied: {
                    notificationBar.success(qsTranslate("notification-bar",
                        "%1 copied to clipboard").arg(description))
                }
                onDisplayTextChanged: {
                    cover.setText(displayText)
                    cover.stringMode = hashDisplay.stringMode
                    }
            }

            Rectangle {
                width: parent.width
                height: Theme.paddingLarge
                opacity: 0
            }

        }
    }
}


