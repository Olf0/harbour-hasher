import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: errorPage

    property var title: qsTranslate("error-page","Error")
    property var message: ""

    function copyToClipboard () {
        Clipboard.text = errorMessage.text
        notificationBar.success(qsTranslate("error-page",
            "%1 copied to clipboard").arg(qsTranslate("error-page",
                "Error message")))
    }

    SilicaFlickable {
        id: view
        anchors.fill: parent
        contentHeight: column.height

        PullDownMenu {
            MenuItem {
                text: qsTranslate("first-page","Copy to Clipboard")
                onClicked: copyToClipboard()
            }
        }


        VerticalScrollDecorator {}

        Column {
            id: column
            spacing: Theme.paddingLarge
            width: parent.width

            PageHeader {
                id: header
                width: parent.width
                title: errorPage.title
            }

            TextArea {
                id: errorMessage
                label: qsTranslate("error-page","Error Message")
                placeholderText: qsTranslate("error-page", "Error Message")
                width: parent.width
                text: errorPage.message
                readOnly: true
                onClicked: copyToClipboard()
            }

        }
    }
}



