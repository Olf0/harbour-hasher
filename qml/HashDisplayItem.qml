import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Item {
    id: root
    property var codecsModel: ListModel {}
    property var monoFontFamily: Theme.fontFamily

    property var algorithmParams: undefined
    property var currentlyProcessingParams: undefined

    property var bridgeEncoding: undefined

    property var comboBoxLabel: undefined
    property var comboBoxDescription: undefined
    property var textAreaLabel: undefined
    property var textAreaPlaceholderText: undefined

    property var initialDisplayEncodingElement: function(e){return true}

    property var bridgeHashValue: undefined

    property var displayField: undefined
    property var displayTextArea: undefined
    property var displayText: stringMode ?
        ((displayTextArea||{}).text||"") :
        ((displayField||{}).outputText||"")

    property var busy: false
    property var error: false

    property bool stringMode: false

    signal textCopied(var description)

    height: column.height
    width: parent.width

    Column {
        id: column
        spacing: Theme.paddingMedium
        width: parent.width

        DisplayingTextArea {
            id: displayTextArea
            visible: stringMode
            wrapMode: text.indexOf(" ")!=-1 ? TextEdit.WordWrap
                : TextEdit.WrapAnywhere
            font.family: Theme.fontFamily
            text: bridgeHashValue || ""
            readOnly: true
            label: (algorithmParams||{}).displayname ||
                qsTranslate("textarea-label","Hash")
            Component.onCompleted: {
                root.displayTextArea = displayTextArea
            }
            onClicked: {
                Clipboard.text = text
                root.textCopied(label)
            }
        }

        EncodingTransformer {
            id: displayField
            visible: !stringMode
            active: !stringMode
            enabled: !root.busy
            codecsModel: root.codecsModel
            inputText: displayTextArea.text
            inputEncoding: root.bridgeEncoding
            initialOutputEncodingElement: initialDisplayEncodingElement

            comboBoxLabel: undefined
            comboBoxDescription: qsTranslate("combobox-description",
                "How to display the hash")
            textAreaLabel: root.textAreaLabel || qsTranslate("textarea-label",
                "%1 representation of the %2 hash")
                .arg(displayField.comboBox.value)
                .arg((algorithmParams||{}).name || "")
            textAreaPlaceholderText: root.textAreaPlaceholderText ||
                qsTranslate("textarea-placeholder", "Hash")

            onTextCopied: {
                root.textCopied(description)
            }

            BusyIndicator {
                visible: running
                running: root.busy
                anchors.centerIn: parent
                size: BusyIndicatorSize.Medium
            }

            Component.onCompleted: {
                root.displayField = displayField
            }

        }

    }

    function update () {
        if(!algorithmParams || JSON.stringify(algorithmParams)
                == JSON.stringify(currentlyProcessingParams)
                || ! bridgeEncoding ) {
            return
        }
        currentlyProcessingParams = JSON.parse(JSON.stringify(algorithmParams))
        if(!(algorithmParams.name && algorithmParams.params)) {
            return
        }
        console.debug(
            ("Telling Python to calculate %1-encoded " +
            "%2 digest with params %3")
            .arg(bridgeEncoding)
            .arg(algorithmParams.name)
            .arg(JSON.stringify(algorithmParams))
            )
        busy = true
        python.call("python.app.hash",
            [algorithmParams.name, algorithmParams.params, bridgeEncoding],
            function(hash){
            if(hash === undefined) {
                console.warn(
                    ("Python obviously had a problem calculating %1-encoded " +
                    "%2 digest with params %3")
                    .arg(bridgeEncoding)
                    .arg(algorithmParams.name)
                    .arg(JSON.stringify(algorithmParams))
                    )
                error = true
                bridgeHashValue = ""
            } else {
                console.warn(
                    "Python says %1-encoded %2 digest with params %3 is %4"
                    .arg(bridgeEncoding)
                    .arg(algorithmParams.name)
                    .arg(JSON.stringify(algorithmParams))
                    .arg(hash)
                    )
                error = false
                bridgeHashValue = hash
            }
            busy = false
            currentlyProcessingParams = undefined
        })
    }

    onAlgorithmParamsChanged: {
        // console.log("algorithmParams changed to %1"
        //     .arg(JSON.stringify(algorithmParams)))
        stringMode = true
        bridgeHashValue = ""
        bridgeEncoding = (algorithmParams||{}).returns == "utf8simple"
            ? "utf8simple" : "hexadecimal"
        // console.log("updating because algorithmParams changed")
        update()
        stringMode = bridgeEncoding == "utf8simple"
    }

    Component.onCompleted: {
        // find first monospace font
        Qt.fontFamilies().some(function(family){
            if(family.toLowerCase().indexOf("mono") != -1) {
                monoFontFamily = family
                return true
            }
        })
    }

}
