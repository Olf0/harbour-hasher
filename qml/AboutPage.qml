import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: aboutpage

    SilicaFlickable {
        id: view
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {}

        Column {
            id: column
            spacing: Theme.paddingLarge
            width: parent.width

            PageHeader {
                id: header
                width: parent.width
                title: qsTranslate("about-page","About Hasher")
            }

            Image {
                id: appIcon
                anchors.horizontalCenter: parent.horizontalCenter
                asynchronous: true
                fillMode: Image.PreserveAspectFit
                source: "../images/harbour-hasher.svg"
                sourceSize {
                    width: parent.width / 2
                    height: parent.width / 2
                }
            }

            Label {
                id: appNameLabel
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTranslate("about-page", "Hasher")
                font.bold: true
                font.pixelSize: Theme.fontSizeLarge
            }

            Label {
                id: appVersionLabel
                anchors.horizontalCenter: parent.horizontalCenter
                text: app.version
                font.bold: true
            }

            Label {
                id: appDescriptionLabel
                width: parent.width
                anchors {
                    rightMargin: Theme.horizonalPageMargin
                    leftMargin: Theme.horizonalPageMargin
                    horizontalCenter: parent.horizontalCenter
                }
                horizontalAlignment: Text.AlignHCenter
                text: qsTranslate("about-page",
                    "SailfishOS app to calculate digests")
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.WordWrap
            }

            SectionHeader {
                text: qsTranslate("about-page", "Translations")
            }

            ListModel {
                id: translationModel
                Component.onCompleted: {
                    console.debug("Requesting translators from Python...")
                    python.call("python.app.l10n.translators", [],
                        function(translators){
                        console.debug("Recieved translators from Python: %1"
                            .arg(JSON.stringify(translators)))
                        translators.forEach(
                            function(translator){append(translator)})
                    })
                }
            }

            Repeater {
                model: translationModel
                delegate: Label {
                    id: label
                    textFormat: Text.RichText
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: Theme.fontSizeMedium
                    text : "%1 - %2"
                        .arg(
                            model.language_team_link ?
                            ("<style>a:link{color: %1;}</style>" +
                            "<a href=\"%2\">%3</a>")
                            .arg(Theme.highlightColor)
                            .arg(model.language_team_link)
                            .arg(model.language_translated)
                            : model.language_translated
                            )
                        .arg(
                            model.translator_link ?
                            ("<style>a:link{color: %1;}</style>" +
                            "<a href=\"%2\">%3</a>")
                            .arg(Theme.highlightColor)
                            .arg(model.translator_link)
                            .arg(model.translator)
                            : model.translator
                            )
                    truncationMode: TruncationMode.Fade
                    width: parent.width
                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }

            SectionHeader {
                text: qsTranslate("about-page", "Links")
            }

            Button {
                id: sourceCodeButton
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTranslate("about-page","Source Code")
                onClicked: {
                    Qt.openUrlExternally(
                        "https://gitlab.com/nobodyinperson/harbour-hasher"
                        )
                }
            }

            Button {
                id: donateButton
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTranslate("about-page","Donate")
                onClicked: Qt.openUrlExternally("https://tinyurl.com/yycbh8so")
            }

            Rectangle {
                width: parent.width
                height: Theme.paddingLarge
                opacity: 0
            }

        }
    }
}



