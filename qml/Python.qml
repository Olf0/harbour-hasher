import QtQuick 2.0
import io.thp.pyotherside 1.4

Python {
    // QML doesn't handle nested ListModels well. Instead we just remember the
    // object in a separate variable and look the nested structures up by hand.
    property var hashAlgorithmsHashlibSimple: undefined
    property var hashAlgorithmsHashlibSimpleModel: ListModel {}
    property var hashAlgorithmsComposite: undefined
    property var hashAlgorithmsCompositeModel: ListModel {}
    property var codecsModel: ListModel {
        function getEncoding(name) {
            for(var i = 0; i < count; i++) {
                var encoding = get(i)
                if(encoding.name == name) {
                    return encoding
                }
            }
        }
    }

    Component.onCompleted: {
        addImportPath(Qt.resolvedUrl('..'));
        importModule('python', function () {
            console.debug("Python module imported")
        });
        setHandler("simple-hashlib-algorithms", function(algorithms) {
            console.debug("Recieved simple hashlib algorithms from python: %1"
                    .arg(JSON.stringify(algorithms)))
            hashAlgorithmsHashlibSimple = algorithms
        })
        setHandler("composite-algorithms", function(algorithms) {
            console.debug("Recieved composite algorithms from python: %1"
                    .arg(JSON.stringify(algorithms)))
            hashAlgorithmsComposite = algorithms
        })
        setHandler("codecs", function(codecs) {
            console.debug("Recieved codecs from python: %1"
                .arg(JSON.stringify(codecs)))
            codecs.forEach(function(codec){codecsModel.append(codec)})
        })
    }

    onHashAlgorithmsCompositeChanged: {
        if(hashAlgorithmsComposite) {
            hashAlgorithmsComposite.forEach(function(algo){
                // JSON-serialize all nested structures as ListModel doesn't
                // play well with those
                var obj = JSON.parse(JSON.stringify(algo))
                for(var key in obj) {
                    var value = obj[key]
                    var str = JSON.stringify(value)
                    if(str.indexOf("{") == 0 || str.indexOf("[") == 0) {
                        obj[key] = str
                    }
                }
                hashAlgorithmsCompositeModel.append(obj)
                })
        }
    }

    onHashAlgorithmsHashlibSimpleChanged: {
        if(hashAlgorithmsHashlibSimple) {
            hashAlgorithmsHashlibSimple.forEach(function(algo){
                // JSON-serialize all nested structures as ListModel doesn't
                // play well with those
                var obj = JSON.parse(JSON.stringify(algo))
                for(var key in obj) {
                    var value = obj[key]
                    var str = JSON.stringify(value)
                    if(str.indexOf("{") == 0 || str.indexOf("[") == 0) {
                        obj[key] = str
                    }
                }
                hashAlgorithmsHashlibSimpleModel.append(obj)
                })
        }
    }

    onError: {
        // when an exception is raised, this error handler will be called
        console.log('python error: ' + traceback);
    }

    onReceived: {
        // asychronous messages from Python arrive here
        // in Python, this can be accomplished via pyotherside.send()
        console.debug('got message from python: ' + data);
    }
}

