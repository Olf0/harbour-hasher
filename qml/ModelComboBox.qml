import QtQuick 2.0
import Sailfish.Silica 1.0

ComboBox {
    id: root
    property var selectionModel: ListModel {}
    property var elementToDisplayText: function(e){return JSON.stringify(e)}
    property var elementToRawText:     function(e){return JSON.stringify(e)}
    property var elementEnabled:       function(e){return true}
    property var initialElement:       function(e){return true}
    property var currentElement: (selectionModel.count && currentIndex >= 0) ?
        selectionModel.get(currentIndex) : undefined
    property var rawValue: currentElement ?
        elementToRawText(currentElement) : undefined
    signal invalidItemSelected()
    menu: ContextMenu {
        Repeater {
            model: selectionModel
            delegate: MenuItem {
                text: elementToDisplayText(model)
                enabled: elementEnabled(model)
            }
        }
    }

    onCurrentItemChanged: {
        if(selectionModel.count) {
            if(!currentItem) {
                invalidItemSelected()
            }
        }
    }

    function selectInitialElement () {
        for(var i = 0; i < selectionModel.count; i++) {
            if(initialElement(selectionModel.get(i))) {
                if(currentIndex != i) { currentIndex = i }
                break;
            }
        }
    }

    Component.onCompleted: {
        if(!selectionModel.count) {
            // - currentIndex defaults to 0
            // - when the model is filled, the value is changed, but the
            //   currentIndex stays at 0 (because that's what is is, really)
            // - to also emit the signal that currentIndex changed on the first
            //   model fill, we need this workaround
            selectionModel.countChanged.connect(function(){
                if(selectionModel.count <= 1) {
                    var oldCurrentIndex = currentIndex
                    currentIndex = -1
                    currentIndex = oldCurrentIndex
                }
                if(initialElement(selectionModel.get(selectionModel.count-1))){
                    currentIndex = selectionModel.count - 1
                }
            })
        } else {
            selectInitialElement()
        }
    }
}
