# system modules
import logging
import unittest
import codecs
import types

# internal modules
from . import app
from . import customcodecs
from . import hashing

# external modules


class CustomCodecTest(unittest.TestCase):
    pass


for subcls in filter(
    lambda c: hasattr(c, "test_data"),
    customcodecs.CustomCodec.__subclasses__(),
):

    def create_test_method(cls):
        def test(self):
            operations = {bytes: "decode", str: "encode"}
            for given, converted_should_be in cls.test_data.items():
                with self.subTest(
                    operation=operations.get(type(given), "?"), input=given
                ):
                    converter = getattr(cls, operations.get(type(given), "?"))
                    converted, n = converter(given)
                    self.assertEqual(n, len(given))
                    self.assertEqual(converted, converted_should_be)
                if getattr(cls, "injective", False):
                    with self.subTest(operation="injectivity", input=given):
                        inverter = getattr(
                            cls, operations.get(type(converted), "?")
                        )
                        inverted, n = inverter(converted)
                        self.assertEqual(n, len(converted))
                        self.assertEqual(inverted, given)

        return test

    setattr(
        CustomCodecTest,
        "test_{}".format(subcls.__name__),
        create_test_method(subcls),
    )


class HashAlgorithmTest(unittest.TestCase):
    pass


for subcls in filter(
    lambda c: hasattr(c, "test_data"), hashing.Algorithm.all_subclasses()
):

    def create_test_method(cls):
        def test(self):
            for params, should_be in cls.test_data.items():
                params = dict(params)
                with self.subTest(params=params):
                    calculated = cls(**params)()
                    self.assertEqual(calculated, should_be)

        return test

    setattr(
        HashAlgorithmTest,
        "test_{}".format(subcls.__name__),
        create_test_method(subcls),
    )

    def create_test_method(cls):
        def test(self):
            for params, should_be in cls.test_data.items():
                params = dict(params)
                for codec in customcodecs.CustomCodec.__subclasses__():
                    if not codec.arbitraryencode:
                        continue
                    with self.subTest(params=params, encoding=codec.name):
                        calculated = app.hash(
                            getattr(cls, "name", cls.__name__),
                            params,
                            codec.name,
                        )
                        self.assertEqual(
                            calculated,
                            codec.decode(
                                should_be
                                if isinstance(should_be, bytes)
                                else should_be.encode()
                            )[0],
                        )

        return test

    setattr(
        HashAlgorithmTest,
        "test_{}_QML_interface".format(subcls.__name__),
        create_test_method(subcls),
    )

if __name__ == "__main__":
    unittest.main()
