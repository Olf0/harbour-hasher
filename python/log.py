# system modules
import os
import logging

# internal modules

# external modules

try:
    loglevel_env = os.environ.get("HASHER_LOGLEVEL", "WARNING")
    loglevel = getattr(logging, loglevel_env)
except AttributeError:
    loglevel = logging.WARNING
try:
    logging.basicConfig(level=loglevel)
except ValueError:
    logging.basicConfig(level=logging.WARNING)
