# system modules
import logging
import codecs
import functools
import operator
import traceback

# internal modules
from . import log
from . import l10n
from . import customcodecs
from . import hashing
from .configuration import Configuration

try:
    from . import config as cfg
except ImportError:
    cfg = object()

logger = logging.getLogger(__name__)

# external modules
try:
    import pyotherside
except ImportError:
    logger.warning("Cannot import PyOtherSide, obviously running without QML")
    from . import fakepyotherside as pyotherside


def notify_on_error(decorated_fun):
    """
    Decorator to catch any exception and send it to QML
    """

    @functools.wraps(decorated_fun)
    def wrapper(*args, **kwargs):
        try:
            return decorated_fun(*args, **kwargs)
        except BaseException as e:
            logger.error("Traceback:\n{}".format(traceback.format_exc()))
            pyotherside.send("error", str(e))
            return None

    return wrapper


@notify_on_error
def transform(*args, **kwargs):
    return customcodecs.transform(*args, **kwargs)


@notify_on_error
def hash(algorithm, params, encoding):
    hashfunc = hashing.Algorithm.new(algorithm, params)
    digest = hashfunc()
    hashstring = codecs.decode(
        digest if type(digest) is bytes else codecs.encode(digest, "utf8"),
        encoding,
    )
    return hashstring


logger.debug("Sending codecs {} to QML".format(tuple(customcodecs.info())))
pyotherside.send("codecs", customcodecs.info())

algorithms = sorted(
    hashing.Algorithm.available(), key=hashing.Algorithm.sort_by_name
)

simple_hashlib_algorithms = tuple(
    map(
        operator.methodcaller("metadata"),
        filter(lambda a: getattr(a, "hashlib_simple", False), algorithms),
    )
)
logger.debug(
    "Sending simple hashlib algorithms {} to QML".format(
        simple_hashlib_algorithms
    )
)
pyotherside.send("simple-hashlib-algorithms", simple_hashlib_algorithms)

composite_algorithms = tuple(
    map(
        operator.methodcaller("metadata"),
        filter(lambda a: not getattr(a, "hashlib_simple", False), algorithms),
    )
)
logger.debug(
    "Sending composite algorithms {} to QML".format(composite_algorithms)
)
pyotherside.send("composite-algorithms", composite_algorithms)

logger.debug(
    "Sending app version {} to QML".format(getattr(cfg, "VERSION", "VERSION"))
)
pyotherside.send("app-version", getattr(cfg, "VERSION", "VERSION"))

logger.debug("Reading user configuration")
config = Configuration()
config.read_home()

logger.debug("Python module loaded")
