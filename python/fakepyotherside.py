# system modules
import logging
import itertools

# internal modules

# external modules

logger = logging.getLogger(__name__)


def send(*args, **kwargs):
    logger.info(
        "send({})".format(
            ", ".join(
                itertools.chain(
                    map(repr, args),
                    map(
                        lambda kv: "=".join((kv[0], repr(kv[1]))),
                        kwargs.items(),
                    ),
                )
            )
        )
    )
