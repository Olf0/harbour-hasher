# system modules

# internal modules
from . import log
from . import l10n
from . import app

# from . import hashing
from . import customcodecs
from . import hashing

try:
    from . import config as cfg
except ImportError:
    cfg = object()

__version__ = getattr(cfg, "VERSION", "VERSION")

# external modules
