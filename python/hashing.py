# system modules
import logging
import codecs
import unittest
import warnings
import hashlib
import hmac
import re

# internal modules
from . import l10n
from . import customcodecs

# external modules
import attr

try:
    import pwdhash
except ImportError:
    pwdhash = object()


logger = logging.getLogger(__name__)


def dict2bytes(d):
    if isinstance(d, bytes):
        return d
    text, encoding = map(lambda x: d.get(x, ""), ("text", "encoding"))
    if not encoding:
        logger.warning("No encoding given in {}. Assuming UTF-8.".format(d))
        encoding = "utf8simple"
    return codecs.encode(text, encoding)


@attr.s
class IntRangeValidator:
    minimum = attr.ib(
        default=None,
        validator=attr.validators.optional(attr.validators.instance_of(int)),
    )
    maximum = attr.ib(
        default=None,
        validator=attr.validators.optional(attr.validators.instance_of(int)),
    )

    def __call__(self, obj, attribute, value):
        minimum = float("-inf") if self.minimum is None else self.minimum
        maximum = float("inf") if self.maximum is None else self.maximum
        if not (minimum <= value <= maximum):
            raise ValueError(
                "{} is outside range [{};{}]".format(value, minimum, maximum)
            )


class NoSuchHashMethod(ValueError):
    pass


@attr.s
class Algorithm:
    TYPES = {
        bytes: "bytes",
        attr.NOTHING: "bytes",
        None: "bytes",
        int: "integer",
    }

    @classmethod
    def new(cls, name, params={}):
        subcls = next(
            filter(
                lambda sbcls: getattr(sbcls, "name", sbcls.__name__) == name,
                cls.all_subclasses(),
            ),
            None,
        )
        if not subcls:
            raise NoSuchHashMethod(
                _("No such method: {method}").format(method=name)
            )
        return subcls(**params)

    @classmethod
    def all_subclasses(cls, given_cls=None):
        if given_cls is None:
            given_cls = cls
        for subcls in given_cls.__subclasses__():
            yield subcls
            for subsubcls in cls.all_subclasses(subcls):
                yield subsubcls

    @classmethod
    def attr2dict(cls, attribute):
        d = {
            "name": attribute.name,
            "type": cls.TYPES.get(attribute.type, str(attribute.type)),
        }
        d.update(dict(attribute.metadata))
        if isinstance(attribute.validator, IntRangeValidator):
            d.update(
                dict(
                    map(
                        lambda f: (
                            f.name,
                            getattr(attribute.validator, f.name, None),
                        ),
                        attr.fields(type(attribute.validator)),
                    )
                )
            )
        # due to the QML ListModel type not being able to handle different
        # types per key, we have to wrap it into another object...
        if attribute.default is not attr.NOTHING:
            try:
                d.update(defaultValue={"value": attribute.default()})
            except BaseException:
                d.update(defaultValue={"value": attribute.default})

        return d

    @classmethod
    def available(cls):
        for subcls in cls.all_subclasses():
            try:
                returns = type(subcls()())
            except BaseException as e:
                logger.info(
                    "Skipping {cls.__name__} as it can't be "
                    "instantiated and/or called without arguments".format(
                        cls=subcls
                    )
                )
                continue
            yield subcls

    @staticmethod
    def sort_by_name(cls):
        return getattr(cls, "displayname", getattr(cls, "name", cls.__name__))

    @classmethod
    def metadata(cls):
        try:
            returns = cls.returns
        except AttributeError:
            pass
        return {
            "name": getattr(cls, "name", cls.__name__),
            "displayname": getattr(
                cls, "displayname", getattr(cls, "name", cls.__name__)
            ),
            "returns": {str: "utf8simple"}.get(returns, returns.__name__),
            "description": getattr(cls, "description", None),
            "hashlib_simple": getattr(cls, "hashlib_simple", False),
            "simple": len(attr.fields(cls)) == 1,
            "params": tuple(map(cls.attr2dict, attr.fields(cls))),
        }


@attr.s
class Hash(Algorithm):
    returns = bytes


@attr.s
class HashlibSimpleHash(Hash):
    hashlib_simple = True


@attr.s
class PasswordGenerator(Algorithm):
    returns = str


found = set()
created = set()
for algorithm_name in hashlib.algorithms_available:
    if algorithm_name.lower() in found:
        continue
    try:
        hashlib.new(algorithm_name, b"asdf").digest()
    except BaseException:
        continue
    found.add(algorithm_name.lower())
    clsname = re.sub(r"[^a-zA-Z0-9_]+", "", algorithm_name.upper())
    cls = attr.make_class(
        clsname,
        bases=(HashlibSimpleHash,),
        attrs={
            "data": attr.ib(
                default=b"",
                type=bytes,
                converter=dict2bytes,
                metadata={"displayname": _("Input")},
            )
        },
    )
    cls.name = algorithm_name
    cls.displayname = clsname
    cls.__call__ = lambda s: hashlib.new(s.name, s.data).digest()
    created.add(cls)


@attr.s
class Simple(Hash):
    displayname = _("Simple")
    algorithm = attr.ib(
        default="sha512",
        type="hashlib-simple-algorithm-name",
        metadata={
            "displayname": _("Algorithm"),
            "description": _("Which simple hash algorithm to use."),
        },
    )
    data = attr.ib(
        default=b"",
        type=bytes,
        converter=dict2bytes,
        metadata={"displayname": _("Input")},
    )

    test_data = {
        (("algorithm", "md5"), ("data", b"SailfishOS")): bytes.fromhex(
            "e4ed4a372ea58d1496467fadfa81db53"
        )
    }

    def __call__(self):
        return hashlib.new(self.algorithm, self.data).digest()


if "blake2b" in hashlib.algorithms_available:

    @attr.s
    class Blake2bExtended(Hash):
        name = "blake2bext"
        displayname = "Blake2b ({})".format(_("extended"))
        description = _(
            "This version of Blake2b provides more settings "
            "than the simple version. Still, not all parameters can be set. "
            "Let me know if you need that."
        )
        data = attr.ib(
            default=b"",
            type=bytes,
            converter=dict2bytes,
            metadata={"displayname": _("Input")},
        )
        key = attr.ib(
            default=b"",
            type=bytes,
            converter=dict2bytes,
            metadata={"displayname": _("Key")},
        )
        salt = attr.ib(
            default=b"",
            type=bytes,
            converter=dict2bytes,
            metadata={"displayname": _("Salt")},
        )
        person = attr.ib(
            default=b"",
            type=bytes,
            converter=dict2bytes,
            metadata={
                "displayname": _("Person"),
                "description": _(
                    "This is just an extra string for "
                    "personalization and can be up to 16 bytes long."
                ),
            },
        )
        digest_size = attr.ib(
            default=64,
            type=int,
            converter=int,
            validator=IntRangeValidator(minimum=1, maximum=64),
            metadata={
                "displayname": _("Digest size"),
                "description": _("Resulting digest size in bytes"),
            },
        )

        test_data = {
            (
                ("data", b""),
                ("key", b""),
                ("salt", b""),
                ("person", b""),
                ("digest_size", 16),
            ): bytes.fromhex("cae66941d9efbd404e4d88758ea67670")
        }

        def __call__(self):
            return hashlib.new(
                "blake2b",
                data=self.data,
                person=self.person,
                salt=self.salt,
                key=self.key,
                digest_size=self.digest_size,
            ).digest()


@attr.s
class HMAC(Hash):
    displayname = _("HMAC")
    key = attr.ib(
        default=b"",
        type=bytes,
        converter=dict2bytes,
        metadata={"displayname": _("Key")},
    )
    message = attr.ib(
        default=b"",
        type=bytes,
        converter=dict2bytes,
        metadata={"displayname": _("Message")},
    )
    algorithm = attr.ib(
        default="sha512",
        type="hashlib-simple-algorithm-name",
        metadata={
            "displayname": _("Algorithm"),
            "description": _(
                "Which hash algorithm to "
                "use internally. Currently, this can only be a "
                "simple algorithm without any extra parameters."
            ),
        },
    )

    test_data = {
        (
            ("key", b"secret"),
            ("message", b"string"),
            ("algorithm", "sha1"),
        ): bytes.fromhex("ee8ac90d2d72885a4247f86addea4c3c29a4cba4")
    }
    """
    Calculated on https://www.freeformatter.com/hmac-generator.html
    """

    def __call__(self):
        return hmac.HMAC(
            msg=self.message, key=self.key, digestmod=self.algorithm
        ).digest()


@attr.s
class PBKDF2_HMAC(Hash):
    displayname = _("PBKDF2-HMAC")
    password = attr.ib(
        default=b"",
        type=bytes,
        converter=dict2bytes,
        metadata={
            "displayname": _("Password"),
            "description": _(
                "If you want use PBKDF2-HMAC to derive passwords "
                "from a single master password, "
                "you can enter the master password here."
            ),
            "password": True,
        },
    )
    salt = attr.ib(
        default=b"",
        type=bytes,
        converter=dict2bytes,
        metadata={
            "displayname": _("Salt"),
            "description": _(
                "If you want to derive a password from "
                "your master password entered above, "
                "you can enter your own definition of a login here. "
                "CryptoPass uses username@URL for example."
            ),
        },
    )
    algorithm = attr.ib(
        default="sha512",
        type="hashlib-simple-algorithm-name",
        metadata={
            "displayname": _("Algorithm"),
            "description": _(
                "Which hash algorithm to "
                "use internally for HMAC. Currently, this can only be a "
                "simple algorithm without any extra parameters."
            ),
        },
    )
    iterations = attr.ib(
        default=1,
        type=int,
        converter=int,
        validator=IntRangeValidator(minimum=1, maximum=100000),
        metadata={"displayname": _("Iterations")},
    )
    digest_size = attr.ib(
        default=64,
        type=int,
        converter=int,
        validator=IntRangeValidator(minimum=1, maximum=512),
        metadata={
            "displayname": _("Digest size"),
            "description": _("Resulting digest size in bytes"),
        },
    )

    test_data = {
        (
            ("password", b"password"),
            ("salt", b"salt"),
            ("algorithm", "sha512"),
            ("iterations", 100),
            ("digest_size", 16),
        ): bytes.fromhex("fef7276b107040a0a713bcbec9fd3e19")
    }

    def __call__(self):
        return hashlib.pbkdf2_hmac(
            hash_name=self.algorithm,
            password=self.password,
            salt=self.salt,
            iterations=self.iterations,
            dklen=self.digest_size,
        )


@attr.s
class CryptoPass(PasswordGenerator):
    displayname = _("CryptoPass")
    description = _(
        "This is an implementation of the "
        "PBKDF2-HMAC-based algorithm used in the CryptoPass "
        "password generation tool available as "
        "an Android app and Chrome Extension."
    )
    password = attr.ib(
        default=b"",
        type=bytes,
        converter=dict2bytes,
        metadata={
            "displayname": _("Password"),
            "description": _("Your master password"),
            "password": True,
        },
    )
    username = attr.ib(
        default=b"",
        type=bytes,
        converter=dict2bytes,
        metadata={
            "displayname": _("Username"),
            "description": _(
                "The username for which "
                "you want to generate the password. "
                "You can also leave this empty."
            ),
        },
    )
    url = attr.ib(
        default=b"",
        type=bytes,
        converter=dict2bytes,
        metadata={
            "displayname": _("URL"),
            "description": _(
                "The service for which "
                "you want to generate the password. "
                "You can use the website URL (e.g. twitter.com) "
                "or another descriptive "
                "name that you can remember better (e.g. Twitter)."
                "You can also leave this empty."
            ),
        },
    )
    length = attr.ib(
        default=25,
        type=int,
        converter=int,
        validator=IntRangeValidator(minimum=1, maximum=100),
        metadata={
            "displayname": _("Password Length"),
            "description": _("Resulting password length in characters."),
        },
    )

    test_data = {
        (
            ("password", b"secret"),
            ("username", b"username"),
            ("url", b"internet.com"),
            ("length", 25),
        ): "XK6mbAkrIv402pd7c67h5s23U"
    }
    """
    Calculated with the CryptoPass Chrome Extension
    """

    def __call__(self):
        digest = hashlib.pbkdf2_hmac(
            hash_name="sha256",
            password=self.password,
            salt=self.username + b"@" + self.url,
            iterations=5000,
            dklen=self.length,
        )
        decoded = codecs.decode(digest, "base64simple")
        return decoded[: self.length]


if hasattr(pwdhash, "generate"):

    @attr.s
    class PwdHash(PasswordGenerator):
        displayname = _("PwdHash")
        description = _(
            "This is an implementation of the original "
            "HMAC-MD5-based Stanford PwdHash password mangling scheme which "
            "is also available for Firefox, Chrome and Opera. You can use it "
            "to mangle your actual site passwords as an additional "
            "security measure."
        )
        url = attr.ib(
            default=b"",  # TODO: This should actually be a string
            type=bytes,
            converter=dict2bytes,
            metadata={
                "displayname": _("URL"),
                "description": _(
                    "The site for which you want to mangle your password. "
                    "This is supposed to be a website URL (e.g. twitter.com)."
                ),
                # TODO: maybe some hint that the it can't be empty?
            },
        )
        password = attr.ib(
            default=b"",  # TODO: This should actually be a string
            type=bytes,
            converter=dict2bytes,
            metadata={
                "displayname": _("Password"),
                "description": _("Your actual password for the site."),
                "password": True,
                # TODO: maybe some hint that the it can't be empty?
            },
        )

        test_data = {
            (
                ("password", "asdf".encode()),
                ("url", "http://example.com".encode()),
            ): "7UDOEf"
        }

        def __call__(self):
            if not self.password or not self.url:
                return "!!! {} !!!".format(
                    _("Neither password nor URL must be empty for PwdHash")
                )
            return pwdhash.generate(self.password.decode(), self.url.decode())
