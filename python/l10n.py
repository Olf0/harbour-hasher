# system modules
import os
import re
import gettext
import operator
import glob
import locale
import logging
import urllib.parse

# internal modules
try:
    from . import config as cfg
except ImportError:
    cfg = object()

# external modules

logger = logging.getLogger(__name__)

GETTEXT_DOMAIN = getattr(cfg, "PACKAGE_NAME", "PACKAGE_NAME")
LOCALEDIR = os.path.join(
    getattr(cfg, "datadir", "datadir"),
    getattr(cfg, "PACKAGE_NAME", "PACKAGE_NAME"),
    "locale",
)
locale.setlocale(locale.LC_ALL, "")
for mod in (locale, gettext):
    mod.bindtextdomain(GETTEXT_DOMAIN, LOCALEDIR)
gettext.textdomain(GETTEXT_DOMAIN)
gettext.install(GETTEXT_DOMAIN, localedir=LOCALEDIR)


def languages(domain=GETTEXT_DOMAIN, localedir=LOCALEDIR):
    """
    Return a generator of available language codes
    """
    return map(
        operator.methodcaller("group", 1),  # extract the language code
        filter(
            bool,  # select only successful matches
            map(
                # match the language code folder
                re.compile(r"([^/]+)/LC_MESSAGES/[^/]+.mo").search,
                glob.glob(  # search for *.mo files
                    os.path.join(
                        localedir, "*", "LC_MESSAGES", "{}.mo".format(domain)
                    )
                ),
            ),
        ),
    )


def translations(domain=GETTEXT_DOMAIN, localedir=LOCALEDIR):
    """
    Return a generator of available gettext Translations

    Yields:
        language_code, gettext.translation: language and translation
    """
    for language in languages(domain=GETTEXT_DOMAIN, localedir=LOCALEDIR):
        try:
            yield (
                language,
                gettext.translation(
                    domain=GETTEXT_DOMAIN,
                    localedir=LOCALEDIR,
                    languages=[language],
                ),
            )
        except (IOError, OSError):
            logger.warning(
                "No translation file found for {}".format(repr(language))
            )
            continue


def link2url(link):
    if not link:
        return None
    scheme, netloc, path, params, query, fragment = urllib.parse.urlparse(link)
    if re.search(r"@", path):
        if not (scheme or params or query or fragment):
            scheme = "mailto"
    elif not scheme:
        scheme = "https"
    return urllib.parse.urlunsplit((scheme, netloc, path, params, query))


def translators(**kwargs):
    for language_code, translation in sorted(translations(**kwargs)):
        info = translation.info()
        match = re.match(
            r"\s*(?P<language>[^\s<>]+(?:\s+[^\s<>]+)*)"
            r"(?:\s*<(?P<link>[^>]+)>)?",
            info.get("language-team", ""),
        )
        language_english = (
            match.groupdict().get("language", language_code)
            if match
            else language_code
        )
        language_team_link = (
            link2url(match.groupdict().get("link", "") or "")
            if match
            else None
        )
        match = re.match(
            r"\s*(?P<translator>[^\s<>]+(?:\s+[^\s<>]+)*)"
            r"(?:\s*<(?P<link>[^>]+)>)?",
            info.get("last-translator", ""),
        )
        translator = (
            match.groupdict().get("translator", _("unknown"))
            if match
            else _("unknown")
        )
        translator_link = (
            link2url(match.groupdict().get("link", "") or "")
            if match
            else None
        )
        yield {
            "language_code": language_code,
            "language_english": language_english,
            "translator": translator,
            "language_team_link": language_team_link,
            "translator_link": translator_link,
            "language_translated": {
                "de": _("German"),
                "fr": _("French"),
                "sv": _("Swedish"),
                "zh": _("Chinese"),
            }.get(language_code, language_english),
        }
